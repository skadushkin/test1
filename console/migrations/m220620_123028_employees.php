<?php

use yii\db\Migration;

/**
 * Class m220620_123028_employees
 */
class m220620_123028_employees extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('employees', [
            'id' => $this->primaryKey()->notNull(),
            'user_id' => $this->integer(20)->notNull(),
            'name' => $this->char(250)->notNull(),
            'job' => $this->char(250)->notNull(),
            'department_id' => $this->integer(20)->notNull(),
        ]);

        $this->addForeignKey(
            'fk_user_id',
            'employees',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_department_id',
            'employees',
            'department_id',
            'departments',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey(
            'fk_user_id',
            'employees'
        );
        $this->dropForeignKey(
            'fk_department_id',
            'employees'
        );
        $this->dropTable('employees');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220620_123028_employees cannot be reverted.\n";

        return false;
    }
    */
}
