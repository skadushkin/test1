<?php

use yii\db\Migration;

/**
 * Class m220620_123137_departments
 */
class m220620_122137_departments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('departments', [
            'id' => $this->primaryKey()->notNull(),
            'name' => $this->char(250)->notNull(),
            'description' => $this->char(255)->defaultValue(null),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('departments');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220620_123137_departments cannot be reverted.\n";

        return false;
    }
    */
}
